const express = require('express');

const router = express.Router();

const controller = require('../controllers/controllers');

const mongoose = require('mongoose');
let db_url = 'mongodb://alexandru.dev:alexandru1997@ds145194.mlab.com:45194/certificates_db';

let mongoDB = process.env.MONGODB_URL || db_url;
console.log('db_url = ' + mongoDB);
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

router.post('/adeverinta', controller.saveAdeverinta);
router.get('/adeverinte', controller.getAllAdeverinte);
router.post('/adeverinta/status/:id', controller.changeStatus);

module.exports = router;