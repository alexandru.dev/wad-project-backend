const Adeverinta = require('../models/models');

exports.saveAdeverinta = function(req, res) {
    let adeverinta = new Adeverinta (
        {
            prenume:req.body.prenume,
            nume:req.body.nume,
            an_universitar:req.body.an_universitar,
            data:req.body.data,
            ciclu:req.body.ciclu,
            an_studiu:req.body.an_studiu,
            specializare:req.body.specializare,
            tip_cursuri:req.body.tip,
            mentiuni:req.body.mentiuni,
            motiv:req.body.motiv
        }
    );

    adeverinta.save(function (err) {
        if(err) {
            return next(err);
        }
        res.send('Adeverinta reusita');
    })
};

exports.getAllAdeverinte = function(req, res) {
    Adeverinta.find(function (err, adeverinta) {
        if(err){
            return next(err);
        }
        res.json(adeverinta);
    })
};

 exports.changeStatus = function (req, res) {
     Adeverinta.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
         if(err){
             return next(err);
         }
         res.json(post);
     })
 };
