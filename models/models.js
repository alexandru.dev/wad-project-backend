const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AdeverintaSchema = new Schema({
    prenume: {type: String, require: true, max:100},
    nume: {type: String, require: true, max:100},
    an_studiu: {type: String, require: true, max:100},
    an_universitar: {type: String, require: true, max:100},
    ciclu: {type: String, require: true, max:100},
    data: {type: String, require: true, max:100},
    specializare: {type: String, require: true, max:100},
    tip: {type: String, require: true, max:100},
    mentiuni: {type: String, require: true, max:100},
    motiv: {type: String, require: true, max:100},
    status: {type: String, default: 'PENDING', max:100}
})

module.exports = mongoose.model('Adeverinta', AdeverintaSchema);